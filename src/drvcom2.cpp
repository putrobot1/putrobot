#include "ros/ros.h"
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#include "ros/ros.h"
#include "putrobot/speed.h"
#include "serialib.h"
#include <string.h>

using namespace std;
string portcom;
float lewy, prawy;
int status;

void speedCallback(const putrobot::speed::Ptr msg)
{
	lewy = msg->left;
	prawy = msg->right;

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "drvcom2");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/driver/speed", 1, speedCallback);
  /*ros::Publisher speed_pub = n.advertise<putrobot::speed>("/driver/speed", 1);*/
  serialib LS;
  char Ret;


  n.getParam("/drvcom", portcom);
  ros::Rate loop_rate(30);

  Ret = LS.Open(portcom.c_str(), 57600);

  ROS_INFO("port: %s", portcom.c_str());

    while (ros::ok())
    {
    	LS.WriteChar((char)status);
    	LS.WriteChar((char)(int)lewy);
    	LS.WriteChar((char)(int)prawy);
    	LS.ReadChar((char*)&status,10);
    	ROS_INFO("l: %d, p: %d, s: %d", (int)lewy, (int)prawy, status);

      ros::spinOnce();
      loop_rate.sleep();
    }
    LS.Close();
    return 0;
}
