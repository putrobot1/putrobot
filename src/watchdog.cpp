#include "ros/ros.h"
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
using namespace std;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "sender");
  ros::NodeHandle n;
  ros::Publisher alert = n.advertise<std_msgs::String>("/system/alerts", 1);

    ros::Rate loop_rate(0.5);
    std_msgs::String wiadomosc;
    int i = 0;
    ROS_INFO("Watchdog started!");
    while (ros::ok())
    {


      if(i%2==0) wiadomosc.data = "Test Watchdog - Line 1";
      if(i%2==1) wiadomosc.data = "Test Watchdog - Line 2";
      //if(i%3==2) wiadomosc.data = "Test Watchdog - Line 3";

      alert.publish(wiadomosc);
      i++;

      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


