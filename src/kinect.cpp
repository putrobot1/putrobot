#include <ros/ros.h>
#include <tf/transform_listener.h>
#include "std_msgs/String.h"
//Includes all the headers necessary to use the most common public pieces of the ROS system.
#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <putrobot/speed.h>
#include <stdio.h>
putrobot::speed to_send;
std_msgs::Float64 tilt;
using namespace std;
double tilt_akt;
float x, y, z;
  std::vector< geometry_msgs::TransformStamped > vec_msg;


	void PrintVectorString(vector<string> Vect)
	{
		unsigned int i;
		if (Vect.size()==0)
			cout << "Vector is empty" << endl;
		else{
			cout << "[";
			for (i=0; i < Vect.size(); i++)
				cout << Vect[i] << ", ";
			cout << "]" << endl;
		}
	}
	string FindUserID(vector<string> Vect)
	{
		size_t find;
		string user;
		unsigned int i;
				if (Vect.size()==0)
					cout << "Vector is empty" << endl;
				else{

					for (i=0; i < Vect.size(); i++)
					{
						find=Vect[i].find_first_of("1234567890");
					 if (find!=std::string::npos)
					 {
						user=Vect[i].substr(find,1);
						// cout<<Vect[i]<<endl;
						// cout<< find << endl;

					 }
					}
				}
		return user;
	}
	void tiltCallback(const std_msgs::Float64::ConstPtr& msg)
	{
	tilt_akt= msg->data;
	}

// Node pointer
ros::NodeHandle *nptr;
int main(int argc, char **argv)
{
 	vector<string> frameIDS;
	string userr;

	 ros::init( argc, argv, "polozenie_obiektu" );
	 	ros::NodeHandle n;
	 	nptr = &n;
	 	tf::TransformListener listener;
	 	ros::Subscriber get_tilt = n.subscribe("/tilt_angle",1,tiltCallback);
	 	ros::Publisher send_tilt = n.advertise<std_msgs::Float64>("/tilt_angle", 1);

	 	ros::Publisher send_speed = n.advertise<putrobot::speed>("/camera/speed",1);
	 	ros::Rate loop_rate(35);
	 	while(ros::ok())
	 	{

	 		listener.getFrameStrings(frameIDS);

	 		geometry_msgs::TransformStamped msg;

	 		tf::StampedTransform transform;

			//PrintVectorString(frameIDS);
			userr=FindUserID(frameIDS);
			string name ("/neck_");
			name=name+userr;
			cout<< name <<endl;



	 		try{
				  listener.waitForTransform("/camera_depth_optical_frame", name, ros::Time(0), ros::Duration(5.0) );
                                listener.lookupTransform("/camera_depth_optical_frame", name, ros::Time(0),transform);
                                tf::transformStampedTFToMsg(transform, msg);
                                x=msg.transform.translation.x;
                                y=msg.transform.translation.y;
                                z=msg.transform.translation.z;
 				}
	 				catch (tf::TransformException ex){
	 					ROS_ERROR("%s",ex.what());
						to_send.left=0;
						to_send.right=0;
	 				}
				ROS_INFO("Wartosc X: %.3f",x);
	 	// podazanie za szyja

		
	 			
	 			
	 				if(y>-0.15 && y<0.15)
	 				{
					if(x<2.0)
					{
					ROS_INFO("STOP");
	 				to_send.left=0;
	 				to_send.right=0;
					}
					else if(x>2.0)
					{
						ROS_INFO("Naprzod!");
					to_send.left=0.55;
					to_send.right=0.55;
					}
					else
					{
					ROS_INFO("STOP");
					to_send.left=0;
					to_send.right=0;
					}
	 				}
	 				if(y<-0.15)
	 				{	ROS_INFO("W prawo!");
	 					to_send.left=0.8;
	 					to_send.right=-0.8;
	 				}
	 				if(y>0.15)
	 				{	ROS_INFO("W lewo!");
	 					to_send.left=-0.8;
	 					to_send.right=0.8;
	 				}
				
	 				if(z<0.2 && z>-0.2)
	 				{
	 				ROS_INFO("Nie ruszamy kinecta");
	 				tilt.data=tilt_akt;
	 				}
	 				if (z>0.2)
	 				{
	 				if(tilt.data>=30)
	 				{
	 				ROS_INFO("Kinect max!");
	 				tilt.data=30;
	 				}
	 				else
	 				{
	 				ROS_INFO("Podwyzszamy!");
	 				tilt.data++;
	 				}
	 				}
	 				if (z<-0.2)
	 				{
	 				if(tilt.data<=-30)
	 				{
	 				ROS_INFO("Kinect min!");
	 				tilt.data=-30;
	 				}
	 				else
	 				{
	 				ROS_INFO("Obnizamy!");
	 				tilt.data--;
	 				}
	 				}
	 			send_tilt.publish(tilt);
	 			send_speed.publish(to_send);
	 			ros::spinOnce();
	 			loop_rate.sleep();

	 	}
	 	ros::spin();
	 	delete nptr;
	 	return(0);
}
