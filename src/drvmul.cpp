#include "ros/ros.h"
#include <putrobot/speed.h>
#include <putrobot/axes.h>

#include "geometry_msgs/Twist.h"
#include <math.h>

using namespace std;

putrobot::axes xpadaxes, tabletaxes;
geometry_msgs::Twist cmd_vel;
string maxspeed_s;
double maxspeed;
int active_device;


void xpad_update(const putrobot::axes::Ptr msg)
{
	xpadaxes.axispt = msg->axispt;
	xpadaxes.axislp = msg->axislp;
}
void tablet_update(const putrobot::axes::Ptr msg)
{
	tabletaxes.axispt = msg->axispt;
	tabletaxes.axislp = msg->axislp;
}
void autonom_update(const geometry_msgs::Twist::Ptr msg)
{
	cmd_vel.angular = msg->angular;
	cmd_vel.linear = msg->linear;

}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "drvmul");
	ros::NodeHandle n;
	ros::Subscriber sub_xpad = n.subscribe("/xpad/axes", 1, xpad_update);
	ros::Subscriber sub_tablet = n.subscribe("/tablet/axes", 1, tablet_update);
	ros::Subscriber sub_cmd = n.subscribe("/cmd_vel", 1, autonom_update);
	maxspeed = 0.0;
	ros::Publisher speed_pub = n.advertise<putrobot::speed>("/driver/speed", 1);
	putrobot::speed to_send;

	ros::Rate loop_rate(35);


	while (ros::ok())
	    {
			n.getParam("/driver/maxspeed", maxspeed);
			n.getParam("/drvmul/active", active_device);
			if(maxspeed < 0) {maxspeed = 0; n.setParam("/driver/maxspeed",0);}
			if(maxspeed > 6.2) {maxspeed = 6.2;  n.setParam("/driver/maxspeed",6.2);}
			//przekazywanie danych
			switch(active_device)
			{
				case 1:		//XPAD
					if(xpadaxes.axispt>=0)
					{
						to_send.left = ((xpadaxes.axispt/100.0)*maxspeed) - ((xpadaxes.axislp/100.0)*1.0);
						to_send.right = ((xpadaxes.axispt/100.0)*maxspeed) + ((xpadaxes.axislp/100.0)*1.0);
					}
					else
					{
						to_send.left = ((xpadaxes.axispt/100.0)*maxspeed) + ((xpadaxes.axislp/100.0)*1.0);
						to_send.right = ((xpadaxes.axispt/100.0)*maxspeed) - ((xpadaxes.axislp/100.0)*1.0);
					}

					break;
				case 2://Tablet
					if(tabletaxes.axispt>=-10)
					{
						to_send.left = ((tabletaxes.axispt/100.0)*maxspeed) - ((tabletaxes.axislp/100.0)*1.0);
						to_send.right = ((tabletaxes.axispt/100.0)*maxspeed) + ((tabletaxes.axislp/100.0)*1.0);
					}
					else
					{
						to_send.left = ((tabletaxes.axispt/100.0)*maxspeed) + ((tabletaxes.axislp/100.0)*1.0);
						to_send.right = ((tabletaxes.axispt/100.0)*maxspeed) - ((tabletaxes.axislp/100.0)*1.0);
					}
					break;
				case 3://autonomia
						if(cmd_vel.linear.x <=1.5 && cmd_vel.linear.x >= -1.5 && cmd_vel.angular.z <= 1.5 && cmd_vel.angular.z >= -1.5)
						{
						to_send.left = cmd_vel.linear.x - (cmd_vel.angular.z);
						to_send.right = cmd_vel.linear.x + (cmd_vel.angular.z);
						}
						else
						{
						to_send.left = 0.0;
						to_send.right = 0.0;
						}
						break;
				default: //nieznane
					to_send.left = 0.0;
					to_send.right = 0.0;
					break;
			}

			speed_pub.publish(to_send);

			ROS_INFO("DRVMUL->> Active: %d, Sent: L-%2.2f  R-%2.2f, MAX: %0.1f",active_device, to_send.left, to_send.right, maxspeed);

			ros::spinOnce();
			loop_rate.sleep();
	    }
	    return 0;
}
