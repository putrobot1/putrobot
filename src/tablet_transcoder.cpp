#include "ros/ros.h"
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#include "ros/ros.h"
#include <putrobot/speed.h>
#include <putrobot/axes.h>
#include "std_msgs/Float32.h"
#include "geometry_msgs/Twist.h"
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
using namespace cv;

float jpt=0, jlp=0;
float speedL, speedR;
double maxspeed=-1, tmaxspeed;
bool newframe;
Mat camera_frame = cvCreateImage(cvSize(640, 480), IPL_DEPTH_32F, 3);
int active_cam=0, _active_cam=-1;
string command;
bool newCommand, maxspeedc, activecamc, activedevicec;
int newpid =0;
int adevice, acam;
float p,i,d,maxi;
int watchdog, loop;


void joyCallback( const geometry_msgs::Twist::Ptr msg)
{
	jpt = msg->linear.x *100;
	jlp = msg->linear.y *-100;
	watchdog++ ;
}

void call_set_p( const std_msgs::Float32::Ptr& msg)
{
 p = msg->data;
 newpid ++;
}
void call_set_i( const std_msgs::Float32::Ptr& msg)
{
 i = msg->data;
 newpid ++;
}
void call_set_d( const std_msgs::Float32::Ptr& msg)
{
 d = msg->data;
 newpid ++;
}
void call_set_maxi( const std_msgs::Float32::Ptr& msg)
{
 maxi = msg->data;
 newpid ++;
}


void tabcall( const std_msgs::Float32::Ptr& msg)
{

	switch((int)msg->data)
	{
		case 1:
		if(tmaxspeed>=0.6) tmaxspeed -= 0.5;
		maxspeedc = true;
		break;
		case 2:
		if(tmaxspeed<=5.5) tmaxspeed += 0.5;
		maxspeedc=true;
		break;
		case 3:
		if(acam>0) acam -= 1;
		activecamc = true;
		break;
		case 4:
		if(acam<4) acam += 1;
		activecamc=true;
		break;
		case 5:
		adevice = 2;
		activedevicec = true;
		break;
		case 6:
		adevice = 1;
		activedevicec = true;
		break;
		case 7:
		adevice = 3;
		activedevicec = true;
		break;

		case 100:
		_active_cam=-1;
		maxspeed=-1;
		break;

	}
}


void imageCallback0( const sensor_msgs::Image::ConstPtr& image )
{
	cv_bridge::CvImagePtr cv_ptr;
		try{cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);}
		catch (cv_bridge::Exception& e)	{ROS_ERROR("blad::cv_bridge : %s", e.what());return;}

		if(active_cam==0){camera_frame = cv_ptr->image;	newframe = true;}
}
void imageCallback1( const sensor_msgs::Image::ConstPtr& image )
{
	cv_bridge::CvImagePtr cv_ptr;
		try{cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);}
		catch (cv_bridge::Exception& e)	{ROS_ERROR("blad::cv_bridge : %s", e.what());return;}

		if(active_cam==1){camera_frame = cv_ptr->image;	newframe = true;}
}
void imageCallback2( const sensor_msgs::Image::ConstPtr& image )
{
	cv_bridge::CvImagePtr cv_ptr;
		try{cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);}
		catch (cv_bridge::Exception& e)	{ROS_ERROR("blad::cv_bridge : %s", e.what());return;}

		if(active_cam==2){camera_frame = cv_ptr->image;	newframe = true;}
}
void imageCallback3( const sensor_msgs::Image::ConstPtr& image )
{
	cv_bridge::CvImagePtr cv_ptr;
		try{cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);}
		catch (cv_bridge::Exception& e)	{ROS_ERROR("blad::cv_bridge : %s", e.what());return;}

		if(active_cam==3){camera_frame = cv_ptr->image;	newframe = true;}
}
void imageCallback4( const sensor_msgs::Image::ConstPtr& image )
{
	cv_bridge::CvImagePtr cv_ptr;
		try{cv_ptr = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);}
		catch (cv_bridge::Exception& e)	{ROS_ERROR("blad::cv_bridge : %s", e.what());return;}

		if(active_cam==4){camera_frame = cv_ptr->image;	newframe = true;}
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "tablet_transcoder");
  ros::NodeHandle n;
  int64 frameid;
  cv_bridge::CvImage out_msg;
    //image_transport::ImageTransport it(n);
    //image_transport::Publisher pub = it.advertise("camera/image", 1);
    //cvStartWindowThread();

    image_transport::ImageTransport it(n);

    string topic_name;
    n.getParam("/tablet/Cam0",topic_name);
    image_transport::Subscriber sub0 = it.subscribe(topic_name, 1, imageCallback0);
    n.getParam("/tablet/Cam1",topic_name);
    image_transport::Subscriber sub1 = it.subscribe(topic_name, 1, imageCallback1);
    n.getParam("/tablet/Cam2",topic_name);
    image_transport::Subscriber sub2 = it.subscribe(topic_name, 1, imageCallback2);
    n.getParam("/tablet/Cam3",topic_name);
    image_transport::Subscriber sub3 = it.subscribe(topic_name, 1, imageCallback3);
    n.getParam("/tablet/Cam4",topic_name);
    image_transport::Subscriber sub4 = it.subscribe(topic_name, 1, imageCallback4);

    image_transport::Publisher pub = it.advertise("tablet/Image/", 1);


  ros::Publisher speed_pub_l = n.advertise<std_msgs::Float32>("/tablet/left", 1);//akt. predkosc
  ros::Publisher speed_pub_r = n.advertise<std_msgs::Float32>("/tablet/right", 1);//akt. predkosc
  ros::Publisher speed_pub_pl = n.advertise<std_msgs::Float32>("/tablet/pleft", 1);//akt. pozycja
  ros::Publisher speed_pub_pr = n.advertise<std_msgs::Float32>("/tablet/pright", 1);//akt. pozycja
  ros::Publisher pub_v = n.advertise<std_msgs::Float32>("/tablet/voltage", 1);//akt. pozycja
  ros::Publisher pub_ms = n.advertise<std_msgs::Float32>("/tablet/maxspeed", 1);//akt. pozycja
  ros::Publisher pub_ac = n.advertise<std_msgs::Float32>("/tablet/activeCam", 1);//akt. pozycja
  ros::Publisher pub_ad = n.advertise<std_msgs::Float32>("/tablet/active_device", 1);//akt. pozycja


  ros::Publisher speed_pub_tablet = n.advertise<putrobot::axes>("/tablet/axes", 1); //Joy przesyła dalej
  ros::Subscriber joy_sub = n.subscribe("/tablet/joy", 1, joyCallback); //Joy z tableta
  ros::Subscriber tab_sub = n.subscribe("/tablet/command", 1, tabcall); //rozkazy z tableta
  ros::Subscriber set_p = n.subscribe("/tablet/set_p", 1, call_set_p); //rozkazy z tableta
  ros::Subscriber set_i = n.subscribe("/tablet/set_i", 1, call_set_i); //rozkazy z tableta
  ros::Subscriber set_d = n.subscribe("/tablet/set_d", 1, call_set_d); //rozkazy z tableta
  ros::Subscriber set_maxi = n.subscribe("/tablet/set_maxi", 1, call_set_maxi); //rozkazy z tableta

    ros::Rate loop_rate(20);
    std_msgs::Float32 msg;
    putrobot::axes joy;
    //cv::namedWindow("okno", CV_WINDOW_AUTOSIZE);
    while (ros::ok())
    {
    	loop ++;
    	//Obsluga komend
    	if(maxspeedc==true)
    	{
    		n.setParam("/driver/maxspeed", tmaxspeed);
    		maxspeedc = false;
    	}
    	if(activecamc==true)
    	{
    		n.setParam("/tablet/Selected_Cam", acam);
    		activecamc = false;
    	}
    	if(activedevicec==true)
		{
    		n.setParam("/drvmul/active", adevice);
			activedevicec = false;
		}

    	//Pobranie aktywnej kamery
		n.getParam("/tablet/Selected_Cam",active_cam);
			  acam = active_cam;
			  msg.data = (double)active_cam;
			  pub_ac.publish(msg);
    	//publikacja parametrów
    	  double temp;
    	  n.getParam("/driver/LEFT_SPEED",temp);
    	  msg.data = temp;
    	  speed_pub_l.publish(msg);
    	  n.getParam("/driver/RIGHT_SPEED",temp);
    	  msg.data = temp;
    	  speed_pub_r.publish(msg);
    	  n.getParam("/driver/RIGHT_POS",temp);
    	  msg.data = temp;
    	  speed_pub_pr.publish(msg);
    	  n.getParam("/driver/LEFT_POS",temp);
    	  msg.data = temp;
    	  speed_pub_pl.publish(msg);
    	  n.getParam("/driver/VOLTAGE",temp);
    	  msg.data = temp;
    	  pub_v.publish(msg);
    	  n.getParam("/drvmul/active",temp);
		  msg.data = temp;
		  pub_ad.publish(msg);

    	  n.getParam("/driver/maxspeed",temp);
          tmaxspeed = temp;
    	  msg.data = temp;
          pub_ms.publish(msg);


    	  //Przesylanie wybranego obrazu
    	  if(newframe == true)
    	  {
			  cv::resize(camera_frame, camera_frame,Size(320,240),1,1,INTER_LINEAR);
    		  out_msg.header.frame_id=frameid;
			  out_msg.encoding = sensor_msgs::image_encodings::TYPE_8UC3; // Or whatever
			  out_msg.image    = camera_frame; // Your cv::Mat
			  pub.publish(out_msg.toImageMsg());
			  newframe = false;
    	  }

    	  //ustawienie parametrów PID
    	  if(newpid >= 4)
    	  {
    		 newpid = 0;
    		 n.setParam("/driver/p", p);
    		 n.setParam("/driver/i", i);
    		 n.setParam("/driver/d", d);
    		 n.setParam("/driver/maxi", maxi);
    	  }
    	  	  bool stop;
            //Pakowanie danych do struktury

    	  	  if(loop >= 7)
    	  	  {
    	  		  loop = 0;
    	  		  if(watchdog < 1)
    	  			  {
    	  			    stop = true;
    	  			    watchdog = 0;
    	  			  }
    	  		  else stop = false;
    	  	  }

    	  	if(stop)
            {
				jpt=0.0;
				jlp=0.0;
            }
            joy.axispt = jpt;
            joy.axislp = -jlp;


            //Publikacja
            speed_pub_tablet.publish(joy);

      ros::spinOnce();

      loop_rate.sleep();
    }
    return 0;
}
