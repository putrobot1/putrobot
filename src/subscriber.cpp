/*
 * subscriber.cpp
 *
 *  Created on: Oct 23, 2012
 *      Author: robot
 */


#include <ros/ros.h>
//Use image_transport for publishing and subscribing to images in ROS
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
//Include some useful constants for image encoding. Refer to: http://www.ros.org/doc/api/sensor_msgs/html/namespacesensor__msgs_1_1image__encodings.html for more info.
#include <sensor_msgs/image_encodings.h>
//Include headers for OpenCV Image processing
#include <opencv2/imgproc/imgproc.hpp>
//Include headers for OpenCV GUI handling
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


void imageCallback(const sensor_msgs::ImageConstPtr& original_image)
{cv_bridge::CvImagePtr cv_ptr;
try
	{
		//Always copy, returning a mutable CvImage
		//OpenCV expects color images to use BGR channel order.
		cv_ptr = cv_bridge::toCvCopy(original_image, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e)
	{
		//if there is an error during conversion, display it
		ROS_ERROR("subscriber::main.cpp::cv_bridge exception: %s", e.what());
		return;
	}
imshow("people detector",cv_ptr->image);
cv::waitKey(3);
	}

int main(int argc, char **argv)
{
	ros::init(argc,argv,"camera_subsriber");

	ros::NodeHandle nh;
	image_transport::ImageTransport it(nh);


	cv::namedWindow("transport", CV_WINDOW_AUTOSIZE);
	image_transport::Subscriber sub= it.subscribe("camera/image_processed",1, imageCallback);
	cv::destroyWindow("transport");
	ros::spin();


}

