#include "ros/ros.h"
#include "std_msgs/String.h"
#include <unistd.h>
#include <math.h>
#include <linux/joystick.h>
#include <fcntl.h>
#include <sensor_msgs/Joy.h>
#include "putrobot/speed.h"
#include "putrobot/axes.h"

using namespace std;
using namespace boost;


float axis1, axis2;
int axisfb, axislr;
int w_count, w_count_p, watchdog_on;
bool but1, but2, but3, but4;
bool but1b, but2b, but3b, but4b;

//Watek zerujacy predkosc w przypadku braku aktywnosci.

//
void Callback(const sensor_msgs::Joy::Ptr msg)
{
	axis1 = msg->axes[axisfb] * 100;
	axis2 = msg->axes[axislr] * 100;
	but1 = msg->buttons[1];
	but2 = msg->buttons[0];
	but3 = msg->buttons[2];
	but4 = msg->buttons[3];

	w_count++;

}

int main(int argc, char **argv)
{
	//Inicjalizacja watku nadzorujacego
	w_count = 0;
	w_count_p = 0;
	watchdog_on = 0;

	//Inicjalizacja ROS'a
	ros::init(argc, argv, "xpad");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("/joy", 1, Callback);
	ros::Publisher speed_pub = n.advertise<putrobot::axes>("/xpad/axes", 1);
	n.getParam("/xpad/axis_fb", axisfb);
	n.getParam("/xpad/axis_lr", axislr);
	n.getParam("/xpad/watchdog", watchdog_on);

	ros::Rate loop_rate(35);
    putrobot::axes predkosc;

    int i = 0, w=0;

    while (ros::ok())
    {
      i++;
      //Watchdog
      if(watchdog_on == 1){
      if(w_count == w_count_p) w++; //jeśli brak nowej wiadomosci to inkrementuj
      else {w_count_p = w_count; w=0;} //reset licznika dla nowej wiadomosci
      if(w>=20) axis1=axis2=0;
      }


      //Pakowanie danych do struktury
      predkosc.axispt = axis1;
      predkosc.axislp = axis2;

      //Publikacja
      speed_pub.publish(predkosc);

      //Zmiana predkosci
      int active;
      double max_speed;
      int scam;

      n.getParam("/drvmul/active", active);
      if(active == 1) //jesli sterowanie z xpad'a
      {
    	  if(but1==true && but1b == false)//wcisnieto przycisk 1
    	  {
    		  n.getParam("driver/maxspeed", max_speed);
    		  if(max_speed<=6.0)max_speed += 0.5;
    		  n.setParam("driver/maxspeed", max_speed);
    	  }

    	  if(but2==true && but2b == false)//wcisnieto przycisk 1
    	  {
    	      n.getParam("driver/maxspeed", max_speed);
    	      if(max_speed>=1.0)max_speed -= 0.5;
    	      n.setParam("driver/maxspeed", max_speed);
    	  }

    	  if(but3==true && but3b == false)//wcisnieto przycisk 1
    	  {
			  n.getParam("tablet/Selected_Cam", scam);
			  if(scam>0)scam--;
			  n.setParam("tablet/Selected_Cam", scam);
    	  }
    	  if(but4==true && but4b == false)//wcisnieto przycisk 1
    	  {
			  n.getParam("tablet/Selected_Cam", scam);
			  if(scam<4)scam++;
			  n.setParam("tablet/Selected_Cam", scam);
    	  }


    	  //przepisanie stanu aktualnego
    	  but1b = but1;
    	  but2b = but2;
    	  but3b = but3;
    	  but4b = but4;
      }


      if(i>=20)
      {
    	  //co sekundę aktualizacja ustawień;
    	  i=0;
    	  n.getParam("/xpad/axis_fb", axisfb);
    	  n.getParam("/xpad/axis_lr", axislr);
    	  n.getParam("/xpad/watchdog", watchdog_on);
      }


      ros::spinOnce();
      loop_rate.sleep();
    }

    //Zwerowanie wartosci
    predkosc.axispt = 0;
    predkosc.axislp = 0;
    speed_pub.publish(predkosc);
    return 0;
}
