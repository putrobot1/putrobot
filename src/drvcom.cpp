#include "ros/ros.h"
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#include "ros/ros.h"
#include <putrobot/speed.h>
#include "serialib.h"
#include <string.h>
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Quaternion.h"
#include <tf/transform_broadcaster.h>

using namespace std;
string portcom;
int status;
double flo;
double prawy, lewy;

int packFloat(void *buf, float x) {
    unsigned char *b = (unsigned char *)buf;
    unsigned char *p = (unsigned char *) &x;
    b[0] = p[0];
    b[1] = p[1];
    b[2] = p[2];
    b[3] = p[3];
    return 4;
}

float bytesToFloat(unsigned char *b0) 
{ 
    unsigned char byte_array[] = { b0[0], b0[1] ,b0[2], b0[3] };
    float result;
    std::copy(reinterpret_cast<const char*>(&byte_array[0]),
              reinterpret_cast<const char*>(&byte_array[4]),
              reinterpret_cast<char*>(&result));
    return result;
}

//Wywoływana, gdy pojawią się nowe dane sterujące
void drvCallback(const putrobot::speed::Ptr msg)
{
	lewy = msg->left;
	prawy = msg->right;

}

int main(int argc, char **argv)
{
	//Inicjalizacja ROS'a
	  ros::init(argc, argv, "drvcom");
	  ros::NodeHandle n;
	  ros::Subscriber speed_sub = n.subscribe("/driver/speed", 1, drvCallback);
	  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>("/odom", 1);
	  tf::TransformBroadcaster odom_broadcaster;

	  ros::Time current_time = ros::Time::now();
	  ros::Time last_time = ros::Time::now();

	  /*ros::Publisher speed_pub = n.advertise<putrobot::speed>("/driver/speed", 1);*/

	  //Deklaracja zmiennych
	  serialib LS;
	  char Ret;



	      //przygotowanie odometrii
	      nav_msgs::Odometry odom;
	      odom.header.frame_id = "odom";
	      //tf::Quaternion odom_quat;
	      geometry_msgs::Quaternion odom_quat;
	      geometry_msgs::TransformStamped odom_trans;

	  //Pobranie danych o połączeniu
	  n.getParam("/drvcom", portcom);
	  ros::Rate loop_rate(35);

	  do
	  {
	  Ret = LS.Open(portcom.c_str(), 115200);
	  if(Ret!=1) {ROS_ERROR("Blad otwarcia portu sterownika. Kod: %d", Ret); sleep(1);}
	  }while(Ret != 1 && ros::ok());
	  ROS_INFO("port: %s", portcom.c_str());

	  //inicjowanie zmiennych buforowych
	  unsigned char bufp[4],buf1[4],buf2[4],buf3[4],buf4[4],buf5[4],buf6[4],buf7[4],buf8[4],buf9[4],buf10[4],bufk[4];
	  unsigned char send_buffer_f[40];
	  unsigned char send_buffer_d[16];
	  unsigned char read_buffer[100];
	  int k = 0;
	  double bufor;
	  double xval, yval, pxval=0, pyval=0, zval, pzval=0;


	  while (ros::ok())
	  {
		  //Przygotowanie odometrii
		  current_time = ros::Time::now();
		  odom.header.stamp = current_time;


		  k++;
		  //Wysylanie wartosci predkosci do silników
		  //Head
		  bufp[0] = 0x2A;
		  bufp[1] = 0x2A;
		  bufp[2] = 0x2A;
		  bufp[3] = 0x2A;
		  //Tail
		  bufk[0] = 0xFF;
		  bufk[1] = 0xFF;
		  bufk[2] = 0xFF;
		  bufk[3] = 0x7F;
		  //prawy
		  packFloat(buf1,prawy);
		  //lewy
		  packFloat(buf2,lewy);

		  //Uzupełnianie bufora danych
		  for(int i=0; i<4; i++)
		  {
			  send_buffer_d[i] = bufp[i];
			  send_buffer_d[i+4] = buf1[i];
			  send_buffer_d[i+8] = buf2[i];
			  send_buffer_d[i+12] = bufk[i];
		  }

		  
		  ROS_INFO("DRVCOM ->> L: %0.3f, P:%0.3f", lewy, prawy);

		  LS.Write(send_buffer_d, 16); //Wysłanie danych do sterownika silników

		  if(k>50) //Wysyłanie parametrów co sekundę:
		  {
			  //ROdzaj paczki
			  bufp[0] = 0xBA;
			  bufp[1] = 0xBA;
			  bufp[2] = 0xBA;
			  bufp[3] = 0xBA;
			  //Koniec paczki
			  bufk[0] = 0xFF;
			  bufk[1] = 0xFF;
			  bufk[2] = 0xFF;
			  bufk[3] = 0x7F;
			  //Pakowanie pierwszego parametru
			  n.getParam("/driver/p", bufor);
			  packFloat(buf1,bufor);
			  //Pakowanie pierwszego parametru
			  n.getParam("/driver/i", bufor);
			  packFloat(buf2,bufor);
			  //Pakowanie pierwszego parametru
			  n.getParam("/driver/d", bufor);
			  packFloat(buf3,bufor);
			  //Pakowanie pierwszego parametru
			  n.getParam("/driver/max_i", bufor);
			  packFloat(buf4,bufor);

			  //Budowanie dużej struktury
			  for(int i=0; i<4; i++)
			  {
				  send_buffer_f[i] = bufp[i];
				  send_buffer_f[i+4] = buf1[i];
				  send_buffer_f[i+8] = buf2[i];
				  send_buffer_f[i+12] = buf3[i];
				  send_buffer_f[i+16] = buf4[i];
				  send_buffer_f[i+20] = bufk[i];
			  }

			  LS.Write(send_buffer_f,24); //Wysłanie ustawień do silników
			  k=0;
		  }//Koniec wysyłania parametrów co sekundę
		
			
			  LS.Read(read_buffer,50,1);
			  if(read_buffer[3] == 0x7D)
			  {
			  			
				  for(int i=0; i<4; i++)
				  {
					  buf1[i] = read_buffer[i+4];
					  buf2[i] = read_buffer[i+8];
					  buf3[i] = read_buffer[i+12];
					  buf4[i] = read_buffer[i+16];
					  buf5[i] = read_buffer[i+20];
					  buf6[i] = read_buffer[i+24];
					  buf7[i] = read_buffer[i+28];
					  buf8[i] = read_buffer[i+32];
					  buf9[i] = read_buffer[i+36];
					  buf10[i] = read_buffer[i+40];
					  bufk[i] = read_buffer[i+44];
				  }				 
				  flo = bytesToFloat(buf2);				  
				  n.setParam("/driver/RIGHT_SPEED",flo);
				  flo = bytesToFloat(buf3); //pozycja - lewy
				  n.setParam("/driver/LEFT_SPEED",flo);
				  flo = bytesToFloat(buf4); //predkosc - prawy
				  n.setParam("/driver/RIGHT_POS",flo);
				  flo = bytesToFloat(buf5); //predkosc - lewy
				  n.setParam("/driver/LEFT_POS",flo);
				  flo = bytesToFloat(buf6); //Kat obrotu
				  n.setParam("/driver/ANGLE",flo);
				  zval = flo;
				  flo = bytesToFloat(buf7); //X
				  n.setParam("/driver/X",flo);
				  xval = flo;
				  flo = bytesToFloat(buf8); //Y
				  n.setParam("/driver/Y",flo);
				  yval = flo;
				  flo = bytesToFloat(buf9); //DROGA
				  n.setParam("/driver/DISTANCE",flo);
				  flo = bytesToFloat(buf10); //Napięcie
				  n.setParam("/driver/VOLTAGE",flo);


				  //set the position
				  odom.pose.pose.position.z = 0.0;
				  odom.pose.pose.position.x = xval;
				  odom.pose.pose.position.y = -yval;
				  odom.pose.pose.orientation.z =zval;

				  //set the velocity
				  double dt = (current_time - last_time).toSec();
				  odom.child_frame_id = "base_link";
				  odom.twist.twist.linear.x = (xval-pxval)*dt;
				  odom.twist.twist.linear.y = -(yval-pyval)*dt;
				  odom.twist.twist.angular.z = (zval-pzval)*dt;


				  bool is_nan = false;
				  bool is_bad_value = false;
				  //to nie ma szans dzialac!
				  odom_quat = tf::createQuaternionMsgFromYaw(zval);

				  odom_trans.header.stamp = current_time;
				  odom_trans.header.frame_id = "odom";
				  odom_trans.child_frame_id = "base_link";

				  odom_trans.transform.translation.x = xval;
				  odom_trans.transform.translation.y = -yval;
				  odom_trans.transform.translation.z = 0.0;

				  //zmiana formatów
				  /*odom_quatg.w = odom_quat.getW();
				  odom_quatg.x = odom_quat.getX();
				  odom_quatg.y = odom_quat.getY();
				  odom_quatg.z = odom_quat.getZ();*/

				  odom_trans.transform.rotation = odom_quat;

				  if(isnan(odom_trans.transform.rotation.w)) is_nan = true;
				  if(isnan(odom_trans.transform.rotation.x)) is_nan = true;
				  if(isnan(odom_trans.transform.rotation.y)) is_nan = true;
				  if(isnan(odom_trans.transform.rotation.z)) is_nan = true;
				  if(isnan(odom_trans.transform.translation.x)) is_nan = true;
				  if(isnan(odom_trans.transform.translation.y)) is_nan = true;
				  if(isnan(odom_trans.transform.translation.z)) is_nan = true;

				  if(!is_nan)
				  {
					  if(sqrt((double)pow(((xval-pxval)*dt),2)+(double)pow(((yval-pyval)*dt),2))>2.0)
					  {
						  is_bad_value = true;
						  ROS_ERROR("Zbyt duży skok odometrii!");
					  }

				  }
				  else
				  {
					  ROS_ERROR("NaN w odometrii!");
				  }




				  if(!is_nan && !is_bad_value)  odom_broadcaster.sendTransform(odom_trans);

				  //publish the message
				  if(!is_nan && !is_bad_value)  odom_pub.publish(odom);

				  if(!is_nan)
				  {
				  pzval = zval;
				  pxval = xval;
				  pyval = yval;
				  last_time = current_time;
				  }


			  }
			 if(read_buffer[3] == 0x7C)
			  {
				  for(int i=0; i<4; i++)
				  {
					  buf1[i] = read_buffer[i+4];
					  buf2[i] = read_buffer[i+8];
					  buf3[i] = read_buffer[i+12];
					  buf4[i] = read_buffer[i+16];
					  buf5[i] = read_buffer[i+20];
					  bufk[i] = read_buffer[i+24];
				  }
				  flo = bytesToFloat(buf2); //P
				  n.setParam("/driver/P",flo);
				  flo = bytesToFloat(buf3); //I
				  n.setParam("/driver/I",flo);
				  flo = bytesToFloat(buf4); //P
				  n.setParam("/driver/D",flo);
				  flo = bytesToFloat(buf5); //P
				  n.setParam("/driver/WINDUP",flo);

			}

			  read_buffer[3]=0;
		  
		  	ros::spinOnce();
			loop_rate.sleep();
	  }
	  LS.Close();
	  return 0;
}


