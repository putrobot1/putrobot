#include "ros/ros.h"
#include <unistd.h>
#include <math.h>
#include <fcntl.h>
#include "ros/ros.h"
#include "putrobot/speed.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "sender");
  ros::NodeHandle n;
  ros::Publisher speed_pub = n.advertise<putrobot::speed>("/driver/speed", 1);

    ros::Rate loop_rate(20);
    putrobot::speed predkosc;
    int i = 0;
    while (ros::ok())
    {
      predkosc.left = sin(i*3.14/180)*100;
      predkosc.right = cos(i*3.14/180)*100;
      ROS_INFO("Wartosc %f %f", predkosc.left, predkosc.right);
      speed_pub.publish(predkosc);
      i++;
      if(i>=360) i=0;
      ros::spinOnce();

      loop_rate.sleep();
    }
    return 0;
}
