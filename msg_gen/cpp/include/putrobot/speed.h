/* Auto-generated by genmsg_cpp for file /home/robot/ros_workspace/putrobot/msg/speed.msg */
#ifndef PUTROBOT_MESSAGE_SPEED_H
#define PUTROBOT_MESSAGE_SPEED_H
#include <string>
#include <vector>
#include <map>
#include <ostream>
#include "ros/serialization.h"
#include "ros/builtin_message_traits.h"
#include "ros/message_operations.h"
#include "ros/time.h"

#include "ros/macros.h"

#include "ros/assert.h"


namespace putrobot
{
template <class ContainerAllocator>
struct speed_ {
  typedef speed_<ContainerAllocator> Type;

  speed_()
  : left(0.0)
  , right(0.0)
  {
  }

  speed_(const ContainerAllocator& _alloc)
  : left(0.0)
  , right(0.0)
  {
  }

  typedef float _left_type;
  float left;

  typedef float _right_type;
  float right;


  typedef boost::shared_ptr< ::putrobot::speed_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::putrobot::speed_<ContainerAllocator>  const> ConstPtr;
  boost::shared_ptr<std::map<std::string, std::string> > __connection_header;
}; // struct speed
typedef  ::putrobot::speed_<std::allocator<void> > speed;

typedef boost::shared_ptr< ::putrobot::speed> speedPtr;
typedef boost::shared_ptr< ::putrobot::speed const> speedConstPtr;


template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const  ::putrobot::speed_<ContainerAllocator> & v)
{
  ros::message_operations::Printer< ::putrobot::speed_<ContainerAllocator> >::stream(s, "", v);
  return s;}

} // namespace putrobot

namespace ros
{
namespace message_traits
{
template<class ContainerAllocator> struct IsMessage< ::putrobot::speed_<ContainerAllocator> > : public TrueType {};
template<class ContainerAllocator> struct IsMessage< ::putrobot::speed_<ContainerAllocator>  const> : public TrueType {};
template<class ContainerAllocator>
struct MD5Sum< ::putrobot::speed_<ContainerAllocator> > {
  static const char* value() 
  {
    return "3a927990ab5d5c3d628e2d52b8533e52";
  }

  static const char* value(const  ::putrobot::speed_<ContainerAllocator> &) { return value(); } 
  static const uint64_t static_value1 = 0x3a927990ab5d5c3dULL;
  static const uint64_t static_value2 = 0x628e2d52b8533e52ULL;
};

template<class ContainerAllocator>
struct DataType< ::putrobot::speed_<ContainerAllocator> > {
  static const char* value() 
  {
    return "putrobot/speed";
  }

  static const char* value(const  ::putrobot::speed_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator>
struct Definition< ::putrobot::speed_<ContainerAllocator> > {
  static const char* value() 
  {
    return "float32 left\n\
float32 right\n\
";
  }

  static const char* value(const  ::putrobot::speed_<ContainerAllocator> &) { return value(); } 
};

template<class ContainerAllocator> struct IsFixedSize< ::putrobot::speed_<ContainerAllocator> > : public TrueType {};
} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

template<class ContainerAllocator> struct Serializer< ::putrobot::speed_<ContainerAllocator> >
{
  template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
  {
    stream.next(m.left);
    stream.next(m.right);
  }

  ROS_DECLARE_ALLINONE_SERIALIZER;
}; // struct speed_
} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::putrobot::speed_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const  ::putrobot::speed_<ContainerAllocator> & v) 
  {
    s << indent << "left: ";
    Printer<float>::stream(s, indent + "  ", v.left);
    s << indent << "right: ";
    Printer<float>::stream(s, indent + "  ", v.right);
  }
};


} // namespace message_operations
} // namespace ros

#endif // PUTROBOT_MESSAGE_SPEED_H

